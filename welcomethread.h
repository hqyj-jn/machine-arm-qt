#ifndef WELCOMETHREAD_H
#define WELCOMETHREAD_H

#include <QThread>
#include <QDateTime>
#include <QDebug>

class WelcomeThread : public QThread
{
    Q_OBJECT
public:
    explicit WelcomeThread(QObject *parent = 0);

protected:
    void run();

signals:
    void valueSignal(int);

public slots:

};

#endif // WELCOMETHREAD_H
