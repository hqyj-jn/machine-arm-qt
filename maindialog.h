#ifndef MAINDIALOG_H
#define MAINDIALOG_H

#include <QtSerialPort/QSerialPort>
#include <QButtonGroup>
#include <QDebug>
#include <QtWidgets>
#include <QMetaEnum>
#include <QDateTime>
#include "string.h"
#include <QTimer>

namespace Ui {
class MainDialog;
}


class MainDialog : public QDialog
{
    Q_OBJECT

public:
    explicit MainDialog(QWidget *parent = 0);
    ~MainDialog();

private:
    Ui::MainDialog *ui;
    QButtonGroup *connectBtnGroup;
    QTimer *timerConnect = NULL;
    QTimer *timerCmd = NULL;
    int progress = 0;
    bool sendRet = true;
    QProgressDialog* progressDialog;
    QProgressDialog* sliderDialog;
    QSerialPort *serialPort;

    enum HostType
    {
        SERVER = 1,
        HAAS = 2,
        NONE = 3
    };

    void initialSignalSlot(); // 初始化信号槽
    void initialWidgets(); // 初始化组件

    enum MessageType
    {
        QUESTION = 1,
        INFORMATION = 2,
        WARNING = 3,
        CRITICAL = 4
    };

    void showMessage(QString msg,MessageType type); // 弹窗提醒
    bool connect2SerialPort(); // 连接到串口

    enum CommandType
    {
        RESET = 1,
        ACTION1 = 2,
        ACTION2 = 3,
        ACTION3 = 4
    };
    bool sendSerialCmd(CommandType); // 向串口发送命令

protected:
    bool eventFilter(QObject* obj,QEvent* event);

private slots:
    void btnConnClickedSlot();
    void connTimeoutSlot();
    void cmdTimeoutSlot();
    void sliderValueChanged(int);
};

#endif // MAINDIALOG_H
