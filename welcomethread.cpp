#include "welcomethread.h"

WelcomeThread::WelcomeThread(QObject *parent) :
    QThread(parent)
{
}

void WelcomeThread::run()
{
    for(int i = 0;i<=100;i++)
    {
        qsrand(QDateTime::currentMSecsSinceEpoch());
        int rand = qrand()%(101-i)+1;
        msleep(rand);
        emit valueSignal(i);
    }
}
