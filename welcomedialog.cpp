#include "welcomedialog.h"
#include "ui_welcomedialog.h"

WelcomeDialog::WelcomeDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::WelcomeDialog)
{
    ui->setupUi(this);

    setWindowFlags(Qt::FramelessWindowHint|Qt::WindowStaysOnTopHint);

    md = new MainDialog;
    loadConfig();

    WelcomeThread* wt = new WelcomeThread(this);
    connect(wt,SIGNAL(valueSignal(int)),this,SLOT(valueSlot(int)));
    wt->start();
}

WelcomeDialog::~WelcomeDialog()
{
    delete ui;
}

void WelcomeDialog::valueSlot(int value)
{
    ui->progressBar->setValue(value);

    switch (value) {
    case 10:
        ui->label_progress->setText("正在初始化网络环境...");
        break;
    case 20:
        ui->label_progress->setText("正在初始化网络环境...");
        break;
    case 30:
        ui->label_progress->setText("正在加载数据库...");
        break;
    case 40:
        ui->label_progress->setText("正在加载数据库...");
        break;
    case 50:
        ui->label_progress->setText("正在加载传感器数据接收模块...");
        break;
    case 60:
        ui->label_progress->setText("正在加载传感器数据接收模块...");
        break;
    case 70:
        ui->label_progress->setText("正在初始化命令发送模块...");
        break;
    case 80:
        ui->label_progress->setText("正在初始化命令发送模块...");
        break;
    case 90:
        ui->label_progress->setText("正在初始化命令发送模块...");
        break;
    case 100:
        md->show();
        close();
    }

}

void WelcomeDialog::loadConfig()
{
    QSettings setting("config.ini",QSettings::IniFormat);
    setting.setIniCodec(QTextCodec::codecForName("UTF-8"));

//    setting.beginWriteArray("WelcomeDialog");
//    // 启动窗口的各个QLabel内容
//    setting.setValue("Application Name","智慧城市系统 V1.0");
//    setting.setValue("WelcomeDialog label_2","彩云盒子");
//    setting.setValue("WelcomeDialog label_5","Color Cloud");
//    setting.setValue("Application windowTitle","智慧城市");
//    setting.setValue("MainDialog Haas Module Visible",false); // 主界面HaaS直连模块的可见性
//    setting.endArray();

    setting.beginReadArray("WelcomeDialog");
    QString appName = setting.value("Application Name").toString();
    qDebug() << "Application Name:" << appName;
    QString label_2Text = setting.value("WelcomeDialog label_2").toString();
    qDebug() << "WelcomeDialog label_2:" << label_2Text;
    QString label_5Text = setting.value("WelcomeDialog label_5").toString();
    qDebug() << "WelcomeDialog label_5:" << label_5Text;
    QString windowTitle = setting.value("Application windowTitle").toString();
    qDebug() << "Application windowTitle:" << windowTitle;
    bool isHaaSExists = setting.value("MainDialog Haas Module Visible").toBool();
    qDebug() << "MainDialog Haas Module Visible:" << isHaaSExists;
    setting.endArray();

    ui->label_appName->setText(appName);
    ui->label_2->setText(label_2Text);
    ui->label_5->setText(label_5Text);
    md->setWindowTitle(windowTitle);
}

