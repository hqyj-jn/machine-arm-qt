#ifndef QSS_H
#define QSS_H
#include <QString>

#define QListWidget_STYTLE (QString("\
QListView\
{\
    background-color: rgb(255, 255, 255);\
    border-radius: 10px;  \
    outline:none\
}\
QListView::item:hover {\
    background-color: #ffcc66;\
}\
QListView::item:selected {\
    background-color: #ff9900;\
}"))

#define QPushButton_STYTLE (QString("\
/*按钮普通态*/\
QPushButton\
{\
    font-family:Microsoft Yahei;\
    /*字体大小为20点*/\
    font-size:14pt;\
    /*字体颜色为白色*/\
    color:white;\
    /*背景颜色*/\
    background-color:#ffcc66;\
    /*边框圆角半径为8像素*/\
    border-radius:8px;\
}\
/*按钮悬停态*/\
QPushButton:hover\
{\
    /*背景颜色*/\
    background-color:rgb(14 , 150 , 254);\
}\
/*按钮按下态*/\
QPushButton:pressed\
{\
    /*背景颜色*/\
    background-color:#ff9900;\
    /*左内边距为3像素，让按下时字向右移动3像素*/\
    padding-left:3px;\
    /*上内边距为3像素，让按下时字向下移动3像素*/\
    padding-top:3px;\
}\
/*按钮禁用态*/\
QPushButton:!enabled {\
    background-color: rgb(150, 150, 150);\
}\
"))

#define QSlider_STYTLE (QString("\
QSlider::groove:horizontal {\
    border: 0px;\
    background-color: #bbb;\
    border-radius:25px;\
    }\
QSlider::handle:horizontal {\
    background-color:rgb(250 , 190 , 120);\
    border-radius:25px;\
    border: 0px;\
    height: 40px;\
    width: 120px;\
    }\
"))

#define QButtonImage_STYTLE (QString("\
/*按钮普通态*/\
QPushButton\
{\
    /*背景颜色*/\
    background-color:rgb(255 , 255 , 255);\
    border:0px;\
}\
/*按钮禁用态*/\
QPushButton:!enabled {\
    background-color: rgb(150, 150, 150);\
}\
"))


#endif // QSS_H
