#ifndef WELCOMEDIALOG_H
#define WELCOMEDIALOG_H

#include <QDialog>
#include <QSettings>
#include <QDebug>
#include <QTextCodec>
#include "welcomethread.h"
#include "maindialog.h"

namespace Ui {
class WelcomeDialog;
}

class WelcomeDialog : public QDialog
{
    Q_OBJECT

public:
    explicit WelcomeDialog(QWidget *parent  = 0);
    ~WelcomeDialog();

private slots:
    void valueSlot(int);

private:
    Ui::WelcomeDialog *ui;
    MainDialog* md;

    void loadConfig(); // 读取配置文件
};

#endif // WELCOMEDIALOG_H
