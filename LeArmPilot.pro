#-------------------------------------------------
#
# Project created by QtCreator 2024-2-23T09:02:18
#
#-------------------------------------------------

QT       += core gui serialport

RC_FILE += config.rc


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets


QT += printsupport multimedia
QMAKE_CXXFLAGS += -std=c++11

TARGET = LeArmPilot
TEMPLATE = app


SOURCES += main.cpp\
    welcomedialog.cpp \
    welcomethread.cpp \
    maindialog.cpp

HEADERS  += \
    welcomedialog.h \
    welcomethread.h \
    maindialog.h \
    qss.h

FORMS    += \
    welcomedialog.ui \
    maindialog.ui

OTHER_FILES += \
    config.rc

RESOURCES += \
    res.qrc


